﻿using System;
using System.Globalization;
using System.Text;

namespace NextBiggerTask
{
    public static class NumberExtension
    {
        /// <summary>
        /// Finds the nearest largest integer consisting of the digits of the given positive integer number and null if no such number exists.
        /// </summary>
        /// <param name="number">Source number.</param>
        /// <returns>
        /// The nearest largest integer consisting of the digits  of the given positive integer and null if no such number exists.
        /// </returns>
        /// <exception cref="ArgumentException">Thrown when source number is less than 0.</exception>
        public static int? NextBiggerThan(int number)
        {
            if (number <= 0)
            {
                throw new ArgumentException("bad one", nameof(number));
            }
            else if (number >= int.MaxValue)
            {
                return null;
            }

            string d = Convert.ToString(number, CultureInfo.CurrentCulture);
            int[] digits = new int[d.Length];
            int i;

            for (int k = 0; k < d.Length; k++)
            {
                digits[k] = Convert.ToInt32(d.Substring(k, 1), CultureInfo.CurrentCulture);
            }

            for (i = digits.Length - 1; i > 0; i--)
            {
                if (digits[i] > digits[i - 1])
                {
                    break;
                }
            }

            if (i == 0)
            {
                return null;
            }
            else
            {
                int x = digits[i - 1], min = i;

                for (int j = i + 1; j < digits.Length; j++)
                {
                    if (digits[j] > x && digits[j] < digits[min])
                    {
                        min = j;
                    }
                }

                int temp = digits[i - 1];
                digits[i - 1] = digits[min];
                digits[min] = temp;

                Array.Sort(digits, i, digits.Length - i);

                StringBuilder sb = new StringBuilder();

                for (int k = 0; k < digits.Length; k++)
                {
                    sb.Append(Convert.ToString(digits[k], CultureInfo.CurrentCulture));
                }

                string res = sb.ToString();

                return Convert.ToInt32(res, CultureInfo.CurrentCulture);
            }
        }
    }
}
